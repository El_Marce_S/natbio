<div class="inner-banner has-base-color-overlay text-center"
	 style="background-color: #90E0EF">
	<div class="container">
		<div class="box">
			<h1>¿QUIENES SOMOS?</h1>
		</div>
	</div>
</div>
<div class="breadcumb-wrapper">
	<div class="container">
		<div class="pull-left">
			<ul class="list-inline link-list">
				<li>
					<a href="index.html">Inicio</a>
				</li>

				<li>
					¿QUIENES SOMOS?
				</li>
			</ul>
		</div>

	</div>
</div>


<section class="about sec-padd2">
	<div class="container">
		<div class="section-title center">
			<h2>Algo sobre nosotros</h2>
			<p align="justify">NATBIO SRL., Es una empresa Boliviana legalmente establecida que dedica su tiempo y
				esfuerzo en desarrollar la gestión ambiental y de Seguridad Ocupacional con sus clientes para el
				desarrollo empresarial y el cuidado del medio ambiente.

				Nos esforzamos en coadyuvar a nuestros clientes a alcanzar satisfactoriamente los estándares ambientales
				normativos y de seguridad, así como brindar soluciones de ingeniería a problemas ambientales y de
				seguridad ocupacional.
			</p>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="content">
					<h2>Nuestra Misión:</h2>
					<div class="text">
						<p align="justify">Brindar asesoría en materia ambiental así como en seguridad e higiene ocupacional, a los
							diferentes sectores productivos, con la finalidad de dar cumplimiento a la normativa
							ambiental vigente, ofreciendo a nuestros clientes ventajas competitivas mediante los
							conocimientos y la expertise de nuestra empresa y sus profesionales, a través de una
							atención personalizada. </p>
					</div>
					<h2>Nuestra Visión:</h2>
					<div class="text">
						<p align="justify">Ser una empresa líder y modelo en el rubro del asesoramiento ambiental y de seguridad
							ocupacional a nivel nacional, ofreciendo el mejor servicio a sus clientes, mediante el uso
							de la tecnología y la experiencia de nuestra empresa, brindando servicios de alto nivel y
							satisfacción total hacia nuestros clientes.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="border-bottom"></div>

