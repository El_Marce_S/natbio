<!--Start contact area-->
<section class="get-touch-area">
	<div class="container">
		<div class="sec-title text-center">
			<h1>Contáctenos</h1>
			<span class="border"></span>
			<p>Si tiene dudas, consultas o sugerencias puede ponerse en contacto con nostros a través de los siguientes
				medios</p>
		</div>
		<section class="contact-form-area">
			<div class="container" id="#contact">
				<div class="row">
					<div class="col-md-8 col-lg-offset-2">
						<div class="default-form-area">
							<form id="contact-form" name="contact_form" class="default-form"
								  action="<?php echo base_url() ?>index.php/welcome/nuevomsg"
								  method="post">
								<div class="row clearfix">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="text" name="name" class="form-control" value=""
												   placeholder="Nombre Completo *" required="">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="email" name="email" class="form-control required email"
												   value=""
												   placeholder="Correo Electronico *" required="">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="text" name="phone" class="form-control" value=""
												   placeholder="Teléfono de Contacto">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="text" name="subject" class="form-control" value=""
												   placeholder="Motivo de Contacto">
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
									<textarea name="message" class="form-control textarea required"
											  placeholder="Su Mensaje...."></textarea>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group center">
											<button id="myBtn" class="thm-btn" type="submit">Enviar Mensaje</button>
										</div>
									</div>

								</div>

								<script>


									var btn = document.getElementById('myBtn');
									btn.addEventListener('click', function () {
										alert('Su mensaje fue enviado correctamente');
										document.location.href ='/';
									});
								</script>
							</form>
						</div>
					</div>


				</div>
			</div>
		</section>
		<div class="row">
			<!--Start single item-->
			<div class="col-md-4">
				<div class="single-item hvr-grow-shadow text-center">
					<div class="icon-holder">
						<span class="icon-signs2"></span>
					</div>
					<div class="text-holder">
						<h3>Visítenos</h3>
						<span class="border"></span>
						<p>OFICINA CENTRAL <br>CALLE MEXICO NRO.1485 PISO 1<br>ESQ. NICOLAS ACOSTA <br>LA PAZ – BOLIVIA
						</p>
					</div>
				</div>
			</div>
			<!--End single item-->
			<!--Start single item-->
			<div class="col-md-4">
				<div class="single-item hvr-grow-shadow text-center">
					<div class="icon-holder">
						<span class="icon-technology"></span>
					</div>
					<div class="text-holder">
						<h3>Contácto inmediato</h3>
						<span class="border"></span>
						<p>Tel: 2317217 – 2597952 – <br>Cel: 71232233 – 77205940 - 71232231 <br>Email: <a href="#">Info@natbiosrl.com</a>
						</p>
					</div>
				</div>
			</div>
			<!--End single item-->
			<!--Start single item-->
			<div class="col-md-4">
				<div class="single-item hvr-grow-shadow text-center">
					<div class="icon-holder">
						<span class="icon-clock"></span>
					</div>
					<div class="text-holder">
						<h3>Horarios de Atención</h3>
						<span class="border"></span>
						<p>Lunes a Viernes: 08.30am a 18.00pm <br>Sabados 08.30 a 12.30 <br>Domingos<a>: Cerrado</a></p>
					</div>
				</div>
			</div>
			<!--End single item-->
		</div>
	</div>
</section>
<!--End contact area-->
<section class="contact-form-area ">
	<div class="container">
		<div class="col-md-1 ">

		</div>
		<div class="col-md-4">
			<div class="form-right-box text-center">
				<div>
					<img src="<?= base_url() ?>assets/images/resource/001.png" alt="Awesome Image"
						 style="height: 200px; width: 200px">
				</div>
				<h4>Ing. Marco A. Peñaranda Morales</h4>
				<span>GERENTE GENERAL</span>
				<p>Experto acreditado en Salud y Seguridad Ocupacional.</p>
				<p>Especialista en Gestión Ambiental Integral y de salud y seguridad ocupacional . <br></p>
				<p>Contacto: <br>
					<a href="mailto:mpenaranda@natbiosrl.com">mpenaranda@natbiosrl.com</a><br>
					Cel. +591 71232233 <br>
					<br></p>

				<div class="border"></div>
				<ul class="social-links">
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 col-lg-offset-1">
			<div class="form-right-box text-center">
				<div>
					<img src="<?= base_url() ?>assets/images/resource/002.png" alt="Awesome Image"
						 style="height: 200px; width: 200px">
				</div>
				<h4>Ing. Francisco Suarez</h4>
				<span>GERENTE DE PROYECTOS Y  CONSULTORIA</span>
				<p>Master en Medio Ambiente MMA
					Master en Administración de Negocios MBA
					.</p>
				<p>Especialista en gestión integral de recursos naturales, y eficiencia empresarial e Industrial.</p>
				<p>Contacto: <br>
					<a href="mailto:fsuarez@natbiosrl.com">fsuarez@natbiosrl.com</a><br>
					Cel. +591 77205940 <br>
					<br></p>
				<div class="border"></div>
				<ul class="social-links">
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>


</section>
<!--Start contact form area-->

<!--End contact form area-->
