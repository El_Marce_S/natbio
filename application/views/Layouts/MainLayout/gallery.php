<section class="gallery sec-padd style-2 massonary-page">
	<div class="container">

		<div class="row filter-layout masonary-layout">
			<?php
			$files = scandir('assets/images/GALERIA/');
			foreach ($files as $file) {
				if ($file == '..' || $file == '...' || $file == '.') {
					null;
				} else {
					echo('

<article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="' . base_url() . 'assets/images/GALERIA/' . $file . '" alt="" style="height: 240px; width: 370px" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    
                                    <a href="' . base_url() . 'index.php/welcome/singlePicture/' . $file . '"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article> 
					'
					);
				}
			}
			?>
		</div>
		<div class="center paddt-50"><a href="#" class="thm-btn">Load More</a></div>
	</div>
</section>
