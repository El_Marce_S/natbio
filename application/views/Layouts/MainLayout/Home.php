





	<section class="service sec-padd3">
		<div class="container">
			<div class="section-title center">
				<h2>We are ECO Green, Our Mission is <span class="thm-color">save water, animals and environment</span>our
					activities are taken around the world.</h2>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-x-12">
					<div class="service-item center">
						<div class="icon-box">
							<span class="icon-can"></span>
						</div>
						<h4>Recycling</h4>
						<p>Praising pain was born & I will give you a complete ac of the all systems, expound the actual
							great.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-x-12">
					<div class="service-item center">
						<div class="icon-box">
							<span class="icon-tool"></span>
						</div>
						<h4>Eco System</h4>
						<p>Praising pain was born & I will give you a complete ac of the all systems, expound the actual
							great.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-x-12">
					<div class="service-item center">
						<div class="icon-box">
							<span class="icon-nature-1"></span>
						</div>
						<h4>Save Water</h4>
						<p>Praising pain was born & I will give you a complete ac of the all systems, expound the actual
							great.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-x-12">
					<div class="service-item center">
						<div class="icon-box">
							<span class="icon-deer"></span>
						</div>
						<h4>Save Animals</h4>
						<p>Praising pain was born & I will give you a complete ac of the all systems, expound the actual
							great.</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="fact-counter style-2 sec-padd" style="background-image: url(images/background/5.jpg);">
		<div class="container">
			<div class="section-title center">
				<h2>Some Interesting Facts</h2>
			</div>
			<div class="row clearfix">
				<div class="counter-outer clearfix">
					<!--Column-->
					<article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn"
							 data-wow-duration="0ms">
						<div class="item">
							<div class="icon"><i class="icon-heart2"></i></div>
							<div class="count-outer"><span class="count-text" data-speed="3000" data-stop="30">0</span>+
							</div>
							<h4 class="counter-title">Year Of Experience</h4>
						</div>

					</article>

					<!--Column-->
					<article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn"
							 data-wow-duration="0ms">
						<div class="item">
							<div class="icon"><i class="icon-money"></i></div>
							<div class="count-outer">$<span class="count-text" data-speed="3000"
															data-stop="34500">0</span></div>
							<h4 class="counter-title">Funds Collected</h4>
						</div>
					</article>

					<!--Column-->
					<article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn"
							 data-wow-duration="0ms">
						<div class="item">
							<div class="icon"><i class="icon-people3"></i></div>
							<div class="count-outer"><span class="count-text" data-speed="3000" data-stop="347">0</span>
							</div>
							<h4 class="counter-title">Volunteers Involved</h4>
						</div>
					</article>

					<!--Column-->
					<article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn"
							 data-wow-duration="0ms">
						<div class="item">
							<div class="icon"><i class="icon-animals"></i></div>
							<div class="count-outer"><span class="count-text" data-speed="3000" data-stop="485">0</span>%
							</div>
							<h4 class="counter-title">Animals Saved</h4>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>


	<section class="about sec-padd2">
		<div class="container">
			<div class="section-title center">
				<h2>Words About Us</h2>
				<p>Every voice counts! Choose campaign, donate and help us change the world</p>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<figure class="img-box">
						<img src="images/resource/8.jpg" alt="">
					</figure>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="content">
						<h2>Together we can make a difference</h2>
						<div class="text">
							<p>When you give to Our Ecogreen, you know your donation is making a difference. Whether you
								are supporting one of our Signature Programs or our carefully curated list of Gifts That
								Give More, our professional staff works hard every day <br>to ensure every dollar has
								impact for the cause of your choice. </p>
						</div>
						<h4>Our Partner</h4>
						<div class="text">
							<p>We partner with over 320 amazing projects worldwide, and have given over $150 million in
								cash and product grants to other groups since 2011. We also operate our own dynamic
								suite of Signature Programs.</p>
						</div>
						<div class="link"><a href="#" class="thm-btn style-2">Join With Us</a></div>
					</div>
				</div>
			</div>

		</div>
	</section>


	<section class="why-chooseus">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="item">
						<div class="inner-box">
							<!--icon-box-->
							<div class="icon_box">
								<span class="icon-shapes"></span>
							</div>
							<a href="3"><h4>Supporting Good Cause</h4></a>
						</div>
						<div class="text"><p>Your contrbution used locally to help charitable causes and support the
								organization, Support only for good causes. </p></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="item">
						<div class="inner-box">
							<!--icon-box-->
							<div class="icon_box">
								<span class="icon-star"></span>
							</div>
							<a href="#"><h4>Most Trusted One</h4></a>
						</div>
						<div class="text"><p>Your contrbution used locally to help charitable causes and support the
								organization, Support only for good causes. </p></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="item">
						<div class="inner-box">
							<!--icon-box-->
							<div class="icon_box">
								<span class="icon-people-1"></span>
							</div>
							<a href="#"><h4>Supporting Good Cause</h4></a>
						</div>
						<div class="text"><p>Your contrbution used locally to help charitable causes and support the
								organization, Support only for good causes. </p></div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="gallery sec-padd3 style-2" style="background-image: url(images/background/8.jpg)" ;>
		<div class="container">
			<div class="section-title">
				<h2>Our Gallery</h2>
			</div>
			<ul class="post-filter style-3 list-inline float_right">
				<li class="active" data-filter=".filter-item">
					<span>View All</span>
				</li>
				<li data-filter=".Ecology">
					<span>Ecology</span>
				</li>
				<li data-filter=".Wild-Animals">
					<span>Wild Animals</span>
				</li>
				<li data-filter=".Recycling">
					<span>Recycling</span>
				</li>
				<li data-filter=".Water">
					<span>Water</span>
				</li>
				<li data-filter=".Pollution">
					<span>Pollution</span>
				</li>
			</ul>

			<div class="row filter-layout">

				<article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
					<div class="item">
						<div class="img-box">
							<img src="images/project/1.jpg" alt="">
							<div class="overlay">
								<div class="inner-box">
									<div class="content-box">
										<a data-group="1" href="images/project/1.jpg" class="img-popup"><i
													class="fa fa-search-plus"></i></a>
										<a href="single-gallery.html"><i class="fa fa-link"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="content center">
							<h4>Environment</h4>
							<p>Pollution</p>
						</div>
					</div>
				</article>
				<article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
					<div class="item">
						<div class="img-box">
							<img src="images/project/2.jpg" alt="">
							<div class="overlay">
								<div class="inner-box">
									<div class="content-box">
										<a data-group="1" href="images/project/2.jpg" class="img-popup"><i
													class="fa fa-search-plus"></i></a>
										<a href="single-gallery.html"><i class="fa fa-link"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="content center">
							<h4>Windmill Power</h4>
							<p>Ecology, Recycling</p>
						</div>
					</div>
				</article>
				<article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
					<div class="item">
						<div class="img-box">
							<img src="images/project/3.jpg" alt="">
							<div class="overlay">
								<div class="inner-box">
									<div class="content-box">
										<a data-group="1" href="images/project/3.jpg" class="img-popup"><i
													class="fa fa-search-plus"></i></a>
										<a href="single-gallery.html"><i class="fa fa-link"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="content center">
							<h4>Save White Tiger</h4>
							<p>Wild Animals</p>
						</div>
					</div>
				</article>
				<article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
					<div class="item">
						<div class="img-box">
							<img src="images/project/4.jpg" alt="">
							<div class="overlay">
								<div class="inner-box">
									<div class="content-box">
										<a data-group="1" href="images/project/4.jpg" class="img-popup"><i
													class="fa fa-search-plus"></i></a>
										<a href="single-gallery.html"><i class="fa fa-link"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="content center">
							<h4>Wiliwili Recycling</h4>
							<p>Recycling</p>
						</div>
					</div>
				</article>
			</div>

		</div>
	</section>


	<section class="urgent-cause2 sec-padd">
		<div class="container">
			<div class="section-title">
				<h2>Fundraising Campaigns</h2>
				<p>You can help lots of people by donating little.</p>
			</div>
			<div class="cause-carousel">
				<div class="item clearfix">
					<figure class="img-box">
						<img src="images/resource/9.jpg" alt="">
						<div class="overlay">
							<div class="inner-box">
								<div class="content-box">
									<button class="thm-btn style-2 donate-box-btn">donate now</button>
								</div>
							</div>
						</div>
					</figure>

					<div class="content">

						<div class="text center">
							<a href="#"><h4 class="title">Wind Power Grows Up</h4></a>
							<p>We are dedicated to ending homelessness by delive- ring life-changing services for change
								the poor childrens life...</p>
						</div>
						<div class="progress-box">
							<div class="bar">
								<div class="bar-inner animated-bar" data-percent="48%">
									<div class="count-text">48%</div>
								</div>
							</div>
						</div>
						<div class="donate clearfix">
							<div class="donate float_left"><span>Goal: $54000 </span></div>
							<div class="donate float_right">Raised: $24000</div>
						</div>

					</div>

				</div>
				<div class="item clearfix">
					<figure class="img-box">
						<img src="images/resource/10.jpg" alt="">
						<div class="overlay">
							<div class="inner-box">
								<div class="content-box">
									<button class="thm-btn style-2 donate-box-btn">donate now</button>
								</div>
							</div>
						</div>
					</figure>

					<div class="content">

						<div class="text center">
							<a href="#"><h4 class="title">Save White Tiger</h4></a>
							<p>We are dedicated to ending homelessness by delive- ring life-changing services for change
								the poor childrens life...</p>
						</div>
						<div class="progress-box">
							<div class="bar">
								<div class="bar-inner animated-bar" data-percent="48%">
									<div class="count-text">48%</div>
								</div>
							</div>
						</div>
						<div class="donate clearfix">
							<div class="donate float_left"><span>Goal: $92000 </span></div>
							<div class="donate float_right">Raised: $69000</div>
						</div>

					</div>

				</div>
				<div class="item clearfix">
					<figure class="img-box">
						<img src="images/resource/11.jpg" alt="">
						<div class="overlay">
							<div class="inner-box">
								<div class="content-box">
									<button class="thm-btn style-2 donate-box-btn">donate now</button>
								</div>
							</div>
						</div>
					</figure>

					<div class="content">

						<div class="text center">
							<a href="#"><h4 class="title">Go Green Movement</h4></a>
							<p>We are dedicated to ending homelessness by delive- ring life-changing services for change
								the poor childrens life...</p>
						</div>
						<div class="progress-box">
							<div class="bar">
								<div class="bar-inner animated-bar" data-percent="48%">
									<div class="count-text">48%</div>
								</div>
							</div>
						</div>
						<div class="donate clearfix">
							<div class="donate float_left"><span>Goal: $78000 </span></div>
							<div class="donate float_right">Raised: $49000</div>
						</div>

					</div>

				</div>

			</div>
		</div>
	</section>

	<section class="event-style1" style="background-image: url(images/background/3.jpg)" ;>
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-10 col-xs-12">
					<div class="section-title">
						<h2>UpComing Events</h2>
					</div>
				</div>
				<div class="col-md-3 col-sm-2 col-xs-12">
					<a href="#" class="thm-btn style-2 float_right">All Events</a>
				</div>
			</div>
			<div class="row">
				<article class="col-md-6 col-sm-12 col-xs-12">
					<div class="item style-1">
						<div class="img-column">
							<figure class="img-holder">
								<a href="single-event.html"><img src="images/resource/1.jpg" alt=""></a>
								<div class="date">21 <br><span>Mar</span></div>
							</figure>
						</div>
						<div class="text-column">
							<div class="lower-content">
								<p>Organizer: Tom Maddy</p>
								<a href="single-event.html"><h4>A Walk for Healthy Environment</h4></a>
								<div class="text">
									<p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augue.| Sed <br>rutrum,
										libero non pretium tristique, arcu mi sollicitudin...</p>
								</div>
							</div>
							<ul class="post-meta list_inline">
								<li><i class="fa fa-clock-o"></i>Started On: 11.00am</li>
								|&nbsp;&nbsp;&nbsp;
								<li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
							</ul>
						</div>
					</div>
				</article>
				<article class="col-md-6 col-sm-12 col-xs-12">
					<div class="item style-2">
						<div class="clearfix">
							<div class="img-column">
								<figure class="img-holder">
									<a href="single-event.html"><img src="images/resource/2.jpg" alt=""></a>
								</figure>
							</div>
							<div class="text-column">
								<div class="lower-content">
									<p>Organizer: Tom Maddy</p>
									<a href="single-event.html"><h4>Recycling Plastic Bottle</h4></a>
									<div class="text">
										<p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augueed rutrum,
											libero non sed our pretium tristique, arcu mi sollicitudin...</p>
									</div>
								</div>
							</div>
						</div>
						<ul class="post-meta list_inline">
							<li><i class="fa fa-clock-o"></i>Started On: 11.00am</li>
							|&nbsp;&nbsp;&nbsp;
							<li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
						</ul>
					</div>
					<div class="item style-2">
						<div class="clearfix">
							<div class="img-column">
								<figure class="img-holder">
									<a href="single-event.html"><img src="images/resource/3.jpg" alt=""></a>
								</figure>
							</div>
							<div class="text-column">
								<div class="lower-content">
									<p>Organizer: Tom Maddy</p>
									<a href="single-event.html"><h4>Green Construction Practice</h4></a>
									<div class="text">
										<p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augueed rutrum,
											libero non sed our pretium tristique, arcu mi sollicitudin...</p>
									</div>
								</div>
							</div>
						</div>
						<ul class="post-meta list_inline">
							<li><i class="fa fa-clock-o"></i>Started On: 11.00am</li>
							|&nbsp;&nbsp;&nbsp;
							<li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
						</ul>
					</div>
				</article>
			</div>
		</div>
	</section>

	<section class="blog-section sec-padd2">
		<div class="container">
			<div class="section-title center">
				<h2>latest news</h2>
			</div>
			<div class="row">
				<article class="col-md-3 col-sm-6 col-xs-12">
					<div class="default-blog-news wow fadeInUp animated animated"
						 style="visibility: visible; animation-name: fadeInUp;">
						<figure class="img-holder">
							<a href="blog-details.html"><img src="images/blog/1.jpg" alt="News"></a>
							<figcaption class="overlay">
								<div class="box">
									<div class="content">
										<a href="blog-details.html"><i class="fa fa-link" aria-hidden="true"></i></a>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="lower-content">
							<div class="date">March 02, 2017</div>
							<div class="post-meta">by fletcher | 14 Comments</div>
							<a href="blog-details.html"><h4>Steps for Save Animals</h4></a>
							<div class="text">
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis sed
									praesentium voluptatum...</p>
							</div>
						</div>
					</div>

				</article>
				<article class="col-md-3 col-sm-6 col-xs-12">
					<div class="default-blog-news wow fadeInUp animated animated"
						 style="visibility: visible; animation-name: fadeInUp;">
						<figure class="img-holder">
							<a href="blog-details.html"><img src="images/blog/2.jpg" alt="News"></a>
							<figcaption class="overlay">
								<div class="box">
									<div class="content">
										<a href="blog-details.html"><i class="fa fa-link" aria-hidden="true"></i></a>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="lower-content">
							<div class="date">January 14, 2017</div>
							<div class="post-meta">by stephen | 22 Comments</div>
							<a href="blog-details.html"><h4>The Ozone Layer</h4></a>
							<div class="text">
								<p>How all this mistaken idea denouncing pleasure & praising pain was born and will give
									you a complete...</p>
							</div>
						</div>
					</div>

				</article>
				<article class="col-md-3 col-sm-6 col-xs-12">
					<div class="default-blog-news wow fadeInUp animated animated"
						 style="visibility: visible; animation-name: fadeInUp;">
						<figure class="img-holder">
							<a href="blog-details.html"><img src="images/blog/3.jpg" alt="News"></a>
							<figcaption class="overlay">
								<div class="box">
									<div class="content">
										<a href="blog-details.html"><i class="fa fa-link" aria-hidden="true"></i></a>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="lower-content">
							<div class="date">August 21, 2016</div>
							<div class="post-meta">by Vincent | 03 Comments</div>
							<a href="blog-details.html"><h4>Dispose Plastic Products</h4></a>
							<div class="text">
								<p>The great explorer of the truth master builder of human happinessone rejects,
									dislikes, or avoids pleasure...</p>
							</div>
						</div>
					</div>

				</article>
				<article class="col-md-3 col-sm-6 col-xs-12">
					<div class="default-blog-news wow fadeInUp animated animated"
						 style="visibility: visible; animation-name: fadeInUp;">
						<figure class="img-holder">
							<a href="blog-details.html"><img src="images/blog/4.jpg" alt="News"></a>
							<figcaption class="overlay">
								<div class="box">
									<div class="content">
										<a href="blog-details.html"><i class="fa fa-link" aria-hidden="true"></i></a>
									</div>
								</div>
							</figcaption>
						</figure>
						<div class="lower-content">
							<div class="date">July 15, 2016</div>
							<div class="post-meta">by fletcher | 14 Comments</div>
							<a href="blog-details.html"><h4>Ideas for Save Energy</h4></a>
							<div class="text">
								<p>Know how to pursue pleasure rationally encounter consequences that extremely painful
									rationally encounter... </p>
							</div>
						</div>
					</div>

				</article>

			</div>
		</div>
	</section>

	<div class="border-bottom"></div>

	<section class="clients-section sec-padd">
		<div class="container">
			<div class="section-title center">
				<h2>our partners</h2>
			</div>
			<div class="client-carousel owl-carousel owl-theme">

				<div class="item tool_tip" title="media partner">
					<img src="images/clients/1.jpg" alt="Awesome Image">
				</div>
				<div class="item tool_tip" title="media partner">
					<img src="images/clients/2.jpg" alt="Awesome Image">
				</div>
				<div class="item tool_tip" title="media partner">
					<img src="images/clients/3.jpg" alt="Awesome Image">
				</div>
				<div class="item tool_tip" title="media partner">
					<img src="images/clients/4.jpg" alt="Awesome Image">
				</div>
				<div class="item tool_tip" title="media partner">
					<img src="images/clients/5.jpg" alt="Awesome Image">
				</div>

			</div>
		</div>
	</section>

	<section class="call-out">
		<div class="container">
			<div class="float_left">
				<h4>Join Our Mission to Improve a Child's Feature, Pet’s Life and Our Planet.</h4>
			</div>
			<div class="float_right">
				<a href="#" class="thm-btn style-3">Get Involeved</a>
			</div>

		</div>
	</section>

	<footer class="main-footer">

		<!--Widgets Section-->
		<div class="widgets-section">
			<div class="container">
				<div class="row">
					<!--Big Column-->
					<div class="big-column col-md-6 col-sm-12 col-xs-12">
						<div class="row clearfix">

							<!--Footer Column-->
							<div class="col-md-6 col-sm-6 col-xs-12">

								<div class="footer-widget about-column">
									<figure class="footer-logo"><a href="index.html"><img src="images/logo/logo2.png"
																						  alt=""></a></figure>

									<div class="text"><p>When you give to us you know your donation is making a
											diffe. </p></div>
									<ul class="contact-info">
										<li><span class="icon-signs"></span>22/121 Apple Street, New York, <br>NY 10012,
											USA
										</li>
										<li><span class="icon-phone-call"></span> Phone: +123-456-7890</li>
										<li><span class="icon-note"></span>Supportus@Eco greenteam.com</li>
									</ul>
								</div>
							</div>
							<!--Footer Column-->
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="footer-widget link-column">
									<div class="section-title">
										<h4>Quick Links</h4>
									</div>
									<div class="widget-content">
										<ul class="list">
											<li><a href="about.html">About Our Eco green</a></li>
											<li><a href="Eco-System.html">Eco System</a></li>
											<li><a href="Organic-Living.html">Organic Living</a></li>
											<li><a href="Good-Nature">Good Nature</a></li>
											<li><a href="testimonial">Testimonials</a></li>
											<li><a href="Events.html">Upcoming Events</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!--Big Column-->
					<div class="big-column col-md-6 col-sm-12 col-xs-12">
						<div class="row clearfix">

							<!--Footer Column-->
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="footer-widget post-column">
									<div class="section-title">
										<h4>Upcoming Events</h4>
									</div>
									<div class="post-list">
										<div class="post">
											<div class="post-thumb"><a href="#"><img src="images/blog/thumb1.jpg"
																					 alt=""></a></div>
											<a href="#"><h5>Marathon 2017: <br>Run for Cancer People</h5></a>
											<div class="post-info"><i class="fa fa-calendar"></i> 15 Mar, 2017</div>
										</div>
										<div class="post">
											<div class="post-thumb"><a href="#"><img src="images/blog/thumb2.jpg"
																					 alt=""></a></div>
											<a href="#"><h5>Let’s walk to the poor <br>children edu...</h5></a>
											<div class="post-info"><i class="fa fa-calendar"></i> 21 Apr, 2017</div>
										</div>

									</div>

								</div>
							</div>

							<!--Footer Column-->
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="footer-widget contact-column">
									<div class="section-title">
										<h4>Subscribe Us</h4>
									</div>
									<h5>Subscribe to our newsletter!</h5>
									<form action="#">
										<input type="email" placeholder="Email address....">
										<button type="submit"><i class="fa fa-paper-plane"></i></button>
									</form>
									<p>We don’t do mail to spam & your mail <br>id is confidential.</p>

									<ul class="social-icon">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										<li><a href="#"><i class="fa fa-feed"></i></a></li>
										<li><a href="#"><i class="fa fa-skype"></i></a></li>
									</ul>
								</div>
							</div>


						</div>
					</div>

				</div>
			</div>
		</div>


	</footer>

	<!--Footer Bottom-->
	<section class="footer-bottom">
		<div class="container">
			<div class="pull-left copy-text">
				<p><a href="#">Copyrights © 2017</a> All Rights Reserved. Powered by <a href="#">Eco green.</a></p>

			</div><!-- /.pull-right -->
			<div class="pull-right get-text">
				<a href="#">Join Us Now!</a>
			</div><!-- /.pull-left -->
		</div><!-- /.container -->
	</section>

	<!-- Scroll Top  -->
	<button class="scroll-top tran3s color2_bg"><span class="fa fa-angle-up"></span></button>
	<!-- preloader  -->
	<div class="preloader"></div>
	<div id="donate-popup" class="donate-popup">
		<div class="close-donate theme-btn"><span class="fa fa-close"></span></div>
		<div class="popup-inner">


			<div class="container">
				<div class="donate-form-area">
					<div class="section-title center">
						<h2>Donation Information</h2>
					</div>

					<h4>How much would you like to donate:</h4>

					<form action="#" class="donate-form default-form">
						<ul class="chicklet-list clearfix">
							<li>
								<input type="radio" id="donate-amount-1" name="donate-amount"/>
								<label for="donate-amount-1" data-amount="1000">$1000</label>
							</li>
							<li>
								<input type="radio" id="donate-amount-2" name="donate-amount" checked="checked"/>
								<label for="donate-amount-2" data-amount="2000">$2000</label>
							</li>
							<li>
								<input type="radio" id="donate-amount-3" name="donate-amount"/>
								<label for="donate-amount-3" data-amount="3000">$3000</label>
							</li>
							<li>
								<input type="radio" id="donate-amount-4" name="donate-amount"/>
								<label for="donate-amount-4" data-amount="4000">$4000</label>
							</li>
							<li>
								<input type="radio" id="donate-amount-5" name="donate-amount"/>
								<label for="donate-amount-5" data-amount="5000">$5000</label>
							</li>
							<li class="other-amount">

								<div class="input-container" data-message="Every dollar you donate helps end hunger.">
									<span>Or</span><input type="text" id="other-amount" placeholder="Other Amount"/>
								</div>
							</li>
						</ul>

						<h3>Donor Information</h3>

						<div class="form-bg">
							<div class="row clearfix">
								<div class="col-md-6 col-sm-6 col-xs-12">

									<div class="form-group">
										<p>Your Name*</p>
										<input type="text" name="fname" placeholder="">
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<p>Email*</p>
										<input type="text" name="fname" placeholder="">
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<p>Address*</p>
										<input type="text" name="fname" placeholder="">
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<p>Phn Num*</p>
										<input type="text" name="fname" placeholder="">
									</div>
								</div>

							</div>
						</div>

						<ul class="payment-option">
							<li>
								<h4>Choose your payment method:</h4>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input name="pay-us" type="checkbox">
										<span>Paypal</span>
									</label>
								</div>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input name="pay-us" type="checkbox">
										<span>Offline Donation</span>
									</label>
								</div>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input name="pay-us" type="checkbox">
										<span>Credit Card</span>
									</label>
								</div>
							</li>
							<li>
								<div class="checkbox">
									<label>
										<input name="pay-us" type="checkbox">
										<span>Debit Card</span>
									</label>
								</div>
							</li>
						</ul>

						<div class="center">
							<button class="thm-btn" type="submit">Donate Now</button>
						</div>


					</form>
				</div>
			</div>


		</div>
	</div>

	<!-- jQuery -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/menu.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/imagezoom.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/jquery.polyglot.language.switcher.js"></script>
	<script src="js/SmoothScroll.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/validation.js"></script>
	<script src="js/wow.js"></script>
	<script src="js/jquery.fitvids.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/isotope.js"></script>
	<script src="js/pie-chart.js"></script>


	<!-- revolution slider js -->
	<script src="js/rev-slider/jquery.themepunch.tools.min.js"></script>
	<script src="js/rev-slider/jquery.themepunch.revolution.min.js"></script>
	<script src="js/rev-slider/revolution.extension.actions.min.js"></script>
	<script src="js/rev-slider/revolution.extension.carousel.min.js"></script>
	<script src="js/rev-slider/revolution.extension.kenburn.min.js"></script>
	<script src="js/rev-slider/revolution.extension.layeranimation.min.js"></script>
	<script src="js/rev-slider/revolution.extension.migration.min.js"></script>
	<script src="js/rev-slider/revolution.extension.navigation.min.js"></script>
	<script src="js/rev-slider/revolution.extension.parallax.min.js"></script>
	<script src="js/rev-slider/revolution.extension.slideanims.min.js"></script>
	<script src="js/rev-slider/revolution.extension.video.min.js"></script>


	<script src="js/custom.js"></script>

</div>

</body>
</html>
