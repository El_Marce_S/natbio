<section class="theme_menu stricky">
	<div class=" pull-left col-md-2">
		<a href="<?php echo base_url() ?>"><img
					src="<?php echo base_url() ?>/assets/images/logo/NatbioLogo.jpeg" alt=""></a>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<nav class="menuzord" id="main_menu">
					<ul class="menuzord-menu">
						<li><a href="<?php echo base_url() ?>">Inicio</a></li>
						<li><a>Empresa</a>
							<ul class="dropdown">
								<li><a href="<?php echo base_url() ?>index.php/welcome/aboutus">¿Quienes Somos?</a></li>
							</ul>
						</li>
						<li><a href="#">Servicios</a>
							<ul class="dropdown">
								<li><a href="#">Industria</a>
									<ul class="dropdown">
										<li><a href="#"> Registros Ambientales Industriales RAI
											</a></li>
										<li><a href="#"> Informe Ambiental Anual IAA
											</a></li>
										<li><a href="#"> Manifiestos Ambientales Industriales MA
											</a></li>
										<li><a href="#"> Plan de Manejo Ambiental PMA
											</a></li>
										<li><a href="#"> Descripción de Proyecto DP
											</a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA
											</a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA
											</a></li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST
											</a></li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP
											</a></li>
										<li><a href="#"></a></li>
									</ul>
								</li>
								<li><a href="#">Mineria</a>
									<ul class="dropdown">
										<li><a href="#">Formulario para actividades mineras con impactos
												ambientales conocidos no significativos EMAP</a>
										</li>
										<li><a href="#">Auditoría de Línea Base Ambiental ALBA</a>
										</li>
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA</a>
										</li>
										<li><a href="#"> Programa de Prevención y Mitigación PPM</a>
										</li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental PASA</a>
										</li>
										<li><a href="#"> Manifiesto Ambiental MA</a>
										</li>
										<li><a href="#"> Monitoreo Ambiental MOA</a>
										</li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico
												Integral EEIA</a>
										</li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico
												Específico EEIA</a>
										</li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST</a>
										</li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias
												Peligrosas LASP</a>
										</li>
									</ul>
								</li>
								<li><a href="">Hidrocarburos</a>
									<ul class="dropdown">
										<li><a href="#"> Planes de Remediación y Re vegetación</a></li>
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA</a></li>
										<li><a href="#"> Programa de Prevención y Mitigación PPM</a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental PASA</a></li>
										<li><a href="#"> Manifiesto Ambiental MA</a></li>
										<li><a href="#"> Monitoreo Ambiental MOA</a></li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico
												Integral EEIA</a>
										</li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico
												Específico EEIA</a>
										</li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST</a>
										</li>
									</ul>
								</li>
								<li><a href="#">Transporte</a>
									<ul class="dropdown">
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#"> Programa de Prevención y Mitigación </a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental </a></li>
										<li><a href="#"> Manifiesto Ambiental </a></li>
										<li><a href="#"> Monitoreo Ambiental </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST </a></li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP </a></li>
									</ul>
								</li>
								<li><a href="#">Infraestructura</a>
									<ul class="dropdown">
										<li><a href="#">Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#">Programa de Prevención y Mitigación PPM </a></li>
										<li><a href="#">Plan de Aplicación y Seguimiento Ambiental PASA </a></li>
										<li><a href="#">Manifiesto Ambiental MA </a></li>
										<li><a href="#">Monitoreo Ambiental MOA </a></li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
										<li><a href="#">Plan de Salud y Seguridad en el Trabajo PSST </a></li>
										<li><a href="#">Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP </a></li>
									</ul>
								</li>
								<li><a href="#">Energía</a>
									<ul class="dropdown">
										<li><a href="#">Formulario de Nivel de Categorización Ambiental FNCA</a></li>
										<li><a href="#">Programa de Prevención y Mitigación</a></li>
										<li><a href="#">Plan de Aplicación y Seguimiento Ambiental</a></li>
										<li><a href="#">Manifiesto Ambiental</a></li>
										<li><a href="#">Monitoreo Ambiental</a></li>
										<li><a href="#">Plan de Salud y Seguridad en el Trabajo PSST</a></li>
										<li><a href="#">Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP</a></li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA</a></li>
										<li><a href="#">Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA</a></li>
									</ul>
								</li>
								<li><a href="#">Comunicación</a>
									<ul class="dropdown">
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#"> Programa de Prevención y Mitigación </a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental </a></li>
										<li><a href="#"> Manifiesto Ambiental </a></li>
										<li><a href="#"> Monitoreo Ambiental </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
									</ul>
								</li>
								<li><a href="#">Salud</a>
									<ul class="dropdown">
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#"> Programa de Prevención y Mitigación </a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental </a></li>
										<li><a href="#"> Manifiesto Ambiental </a></li>
										<li><a href="#"> Monitoreo Ambiental </a></li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST </a></li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
									</ul>
								</li>
								<li><a href="#">Agricultura</a>
									<ul class="dropdown">
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#"> Programa de Prevención y Mitigación </a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental </a></li>
										<li><a href="#"> Manifiesto Ambiental </a></li>
										<li><a href="#"> Monitoreo Ambiental </a></li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST </a></li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
									</ul>
								</li>
								<li><a href="#">Turismo</a>
									<ul class="dropdown">
										<li><a href="#"> Planes de Manejo Ambientales </a></li>
										<li><a href="#"> Formulario de Nivel de Categorización Ambiental FNCA </a></li>
										<li><a href="#"> Programa de Prevención y Mitigación </a></li>
										<li><a href="#"> Plan de Aplicación y Seguimiento Ambiental </a></li>
										<li><a href="#"> Manifiesto Ambiental </a></li>
										<li><a href="#"> Monitoreo Ambiental </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Integral
												EEIA </a></li>
										<li><a href="#"> Estudio de Evaluación de Impacto Ambiental Analítico Específico
												EEIA </a></li>
										<li><a href="#"> Plan de Salud y Seguridad en el Trabajo PSST </a></li>
										<li><a href="#"> Licencia Ambiental para Actividades con Sustancias Peligrosas
												LASP </a></li>
									</ul>
								</li>
								<li><a href="#">Capacitación</a>
									<ul class="dropdown">
										<li><a href="#"> Medio Ambiente </a></li>
										<li><a href="#"> Seguridad Ocupacional </a></li>
										<li><a href="#"> Primeros Auxilios </a></li>
										<li><a href="#"> Prevención contra incendios y simulacros </a></li>
										<li><a href="#"> Sistemas de Gestión </a></li>
									</ul>
								</li>
								<li><a href="#">Salud y Seguridad Ocupacional</a>
									<ul class="dropdown">
										<li><a href="#"> Planes de salud y seguridad Ocupacional </a></li>
										<li><a href="#"> Protocolos de seguridad y bioseguridad </a></li>
										<li><a href="#"> Supervisión de proyectos en Salud y Seguridad Ocupacional </a>
										</li>
										<li><a href="#"> Desinfección de áreas de trabajo </a></li>
										<li><a href="#"> Asesoramiento y Capacitación en salud y seguridad
												ocupacional. </a></li>
									</ul>
								</li>
							</ul>
						</li>

						<li><a href="">Normativa Ambiental</a>
							<ul class="dropdown">
								<?php
								$directory = "assets/documents/";
								//Directory is set;

								$dir = opendir($directory);
								//Opened the directory;
								while ($file = readdir($dir)) {
									//Loops through all the files/directories in our directory;
									if ($file != "." && $file != "..") {
										if (is_dir($file)) echo '<strong>' . $file . '</strong><br>';
										else echo '.<li><a href="' . base_url() . 'index.php/welcome/SingleFolder/' . $file . '">' . $file . '</a>.';
									}
								}
								?>
							</ul>
						</li>

						<li><a href="#">Nuestros Clientes</a>
							<ul class="dropdown">
								<li><a href="#">Industriales</a>
									<ul class="dropdown">
										<li><a href="#">Grupo Alcos</a></li>
										<li><a href="#">Laboratorios Delta SA</a></li>
										<li><a href="#">Industrias Copacabana</a></li>
										<li><a href="#">COPABOL SA</a></li>
										<li><a href="#">Sociedad Industrial y Comercial La Francesa Siclaf SA</a></li>
										<li><a href="#">MATRIPLAST SA</a></li>
										<li><a href="#">PREVIERI</a></li>
										<li><a href="#">INNOBE SRL</a></li>
										<li><a href="#">Cervecería Boliviana Nacional CBN</a></li>
										<li><a href="#">AGRONAT</a></li>
										<li><a href="#">ALBYs</a></li>
										<li><a href="#">EMBOL SA</a></li>
										<li><a href="#">INCERPAZ</a></li>
										<li><a href="#">KLARYT</a></li>
										<li><a href="#">LOMBTIBOL</a></li>
										<li><a href="#">SABOQ</a></li>
									</ul>
								</li>
								<li><a href="#">Construcción</a>
									<ul class="dropdown">
										<li><a href="#"> CINAL LTDA
											</a></li>
										<li><a href="#"> ARQUITECTURA Y ESTILO A&E
											</a></li>
										<li><a href="#"> EROSKY SRL.
											</a></li>
										<li><a href="#"> ARQGEPRO SRL
											</a></li>
										<li><a href="#"> WORKER SRL.
											</a></li>
										<li><a href="#"> LA VITALICIA SEGUROS Y REASEGUROS DE VIDA S.A.
											</a></li>
										<li><a href="#"> BACHI
											</a></li>
									</ul>
								</li>
								<li><a href="#">Energía</a>
									<ul class="dropdown">
										<li><a href="#"> INGELEC S.A.
											</a></li>
										<li><a href="#"> SAN CRISTOBAL TESA S.A.
											</a></li>
										<li><a href="#"> HIDROELÉCTRICA BOLIVIANA
											</a></li>
										<li><a href="#"> SEGUENCOMA GAS SRL
											</a></li>
										<li><a href="#"> SOMOS GAS SRL.
											</a></li>

									</ul>
								</li>
								<li><a href="#">Transporte</a>
									<ul class="dropdown">
										<li><a href="#">Servicio de Transportes Villca</a></li>
									</ul>
								</li>
								<li><a href="#">Minería</a>
									<ul class="dropdown">
										<li><a href="#"> FLOR DE LARECAJA
											</a></li>
										<li><a href="#"> ILLAMPU
											</a></li>
										<li><a href="#"> ROYAL GOLD
											</a></li>
										<li><a href="#"> MI DULCE ENCARNA
											</a></li>
										<li><a href="#"> FRANC MARTIN
											</a></li>
										<li><a href="#"> CHICOTE GRANDE LA AGUADA
											</a></li>
										<li><a href="#"> FORTALEZA
											</a></li>
										<li><a href="#"> 15 DE AGOSTO
											</a></li>
										<li><a href="#"> UNIFICADA VILLA EL CARMEN
											</a></li>
										<li><a href="#"> KARA KARA
											</a></li>
										<li><a href="#"> REAL SANTA ROSA
											</a></li>
										<li><a href="#"> PIONEROS JALANCHA
											</a></li>
										<li><a href="#"> TUNQUI OLIVO
											</a></li>
										<li><a href="#"> 28 DE OCUTBRE LTDA
											</a></li>
										<li><a href="#"> SIETE SUYOS
											</a></li>
										<li><a href="#"> NUEVA FORTUNA
											</a></li>
										<li><a href="#"> FLOR DE LARECAJA
											</a></li>
										<li><a href="#"> SAN PABLO DE LA VILLA
											</a></li>
										<li><a href="#"> FLOR DE ILLAMPU
											</a></li>
										<li><a href="#"> CALISMAR
											</a></li>
										<li><a href="#"> TRIUNFO SOMET
											</a></li>
										<li><a href="#"> UNION FORTALEZA
											</a></li>
										<li><a href="#"> MAC COPACABANA
											</a></li>
										<li><a href="#"> MINAS PAMPA
											</a></li>
										<li><a href="#"> SAN PABLO
											</a></li>
									</ul>
								</li>
								<li><a href="#">Actividades</a>
									<ul class="dropdown">
										<li><a href="#"> HANSA LTDA.</a></li>
										<li><a href="#">Laboratorios Delta SA</a></li>
										<li><a href="#"> INMOBILIARIA KANTUTANI</a>
											<ul class="dropdown">
												<li><a href="#">CEMENTERIO JARDIN</a></li>
												<li><a href="#">HORNO CREMATORIO CEMENTERIO JARDIN</a></li>
												<li><a href="#">CEMENTERIO LOS ANDES</a></li>
											</ul>
										</li>
										<li><a href="#"> CEMENTERIO CAMPOS DE PAZ
											</a></li>
										<li><a href="#"> CEMENTERIO CAMPO CELESTIAL
											</a></li>
									</ul>
								</li>
							</ul>
						</li>

						<li><a href="<?php echo base_url() ?>index.php/welcome/gallery">Galeria</a>

						</li>

						<li><a href="<?php echo base_url() ?>index.php/welcome/contacts">Contactos</a></li>


					</ul>
				</nav>
			</div>


		</div>


	</div>
</section>
