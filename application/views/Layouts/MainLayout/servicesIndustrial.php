<body>

<div class="boxed_wrapper">
	<div class="inner-banner has-base-color-overlay text-center"
		 style="background: url(assets/images/background/4.jpg);">
		<div class="container">
			<div class="box">
				<h1>Servicios Para Industria</h1>
			</div>
		</div>
	</div>

	<section class="default-section sec-padd">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-8 col-sm-12">
					<div class="service">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<img src='<?php echo base_url() ?>index.php/assets/images/icons/industrilaLogo.png' alt="">
									</div>
									<h4>Recycling</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<span class="icon-tool"></span>
									</div>
									<h4>Eco System</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<span class="icon-nature-1"></span>
									</div>
									<h4>Save Water</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<span class="icon-deer"></span>
									</div>
									<h4>Save Animals</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<span class="icon-fruit"></span>
									</div>
									<h4>Save Animals</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-x-12">
								<div class="service-item center">
									<div class="icon-box">
										<span class="icon-nature-2"></span>
									</div>
									<h4>Save Animals</h4>
									<p>Praising pain was born &amp; I will give you a complete ac of the all systems,
										expound the actual great.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>


</div>


</body>
