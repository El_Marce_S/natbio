<div class="row filter-layout">
	<?php
	$files = scandir(utf8_decode(str_replace('%20', ' ', "assets/documents/$foldername")));
	foreach ($files as $file) {
		if ($file == '..' || $file == '...' || $file == '.') {
			null;
		} else {
			if (strpos($file, '.p') || strpos($file, '.d') || strpos($file, '.j') == true) {
				echo '
			<article class="col-md-3 col-sm-6 col-xs-12 filter-item Water" style="padding-top:10px; padding-left: 15px; height: 120px">
            	    <div class="item">
            	    <a href=""></a>
                	   <div class="content-box" align="center">
                	   		<a  href="' . base_url() . 'assets/documents/' . $foldername . '/' . $file . '" download>
                	   				<i class="fa fa-file-pdf-o" style="color: red; font-size: 60px" aria-hidden="true"></i>
							</a>
						</div>
                    <div class="content center" style="padding-top: 10px">
                        <h6><a href="' . base_url() . 'assets/documents/' . $foldername . '/' . $file . '" download>"' . $file . '"</a></h6>
                    </div>
                </div>
            </article> 
            ';
			} else {
				echo '
			<article class="col-md-3 col-sm-6 col-xs-12 filter-item Water" style="padding-top:10px; padding-left: 15px; height: 120px">
            	    <div class="item">
                	   <div class="content-box" align="center">
                	   <a href="">
                	   	<i class="fa fa-folder-open" style="color: #48CAE4; font-size: 60px" aria-hidden="true"></i>
						</a>
						</div>
                    <div class="content center" style="padding-top: 10px">
                        <h4><a href="' . base_url() . 'index.php/welcome/SubFolder001/' . $foldername . '/' . $file . '">"' . $file . '"</a></h4>
                    </div>
                </div>
            </article>
				';
			}
		}
	}
	?>
</div>
