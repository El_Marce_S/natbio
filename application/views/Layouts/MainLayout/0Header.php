<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Natbio SRL</title>
	<!-- mobile responsive meta -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">
	<link rel="apple-touch-icon" sizes="180x180"
		  href="<?php echo base_url() ?>assets/images/fav-icon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/fav-icon/favicon-32x32.png"
		  sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/fav-icon/favicon-16x16.png"
		  sizes="16x16">
</head>
<body>
<div class="boxed_wrapper">
	<div class="top-bar" style="background-color: #48CAE4">
		<div class="container">
			<div class="clearfix">
				<ul class="float_left top-bar-info">
					<li style="color: #023E8A"><i class="icon-phone-call"></i>Telefono: (+591) 22597952</li>
					<li style="color: #023E8A"><i class="icon-e-mail-envelope"></i>natbio-srl@natbiosrl.com</li>
				</ul>
				<div class="right-column float_right">
					<ul class="social list_inline">
						<li style="color: #023E8A"><a href="#" style="color: #023E8A"><i class="fa fa-facebook"></i></a>
						</li>
						<li style="color: #023E8A"><a href="#" style="color: #023E8A"><i class="fa fa-linkedin"></i></a>
						</li>
						<li style="color: #023E8A"><a href="#" style="color: #023E8A"><i class="fa fa-twitter"></i></a>
						</li>
					</ul>
					<form action="https://www.natbiosrl.com/index.php/welcome/contacts#contact">
						<button class="thm-btn ">Enviar Mensaje
						</button>
					</form>

				</div>
			</div>
		</div>
	</div>

