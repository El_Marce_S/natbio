<footer class="main-footer" style="background-color: white">

	<!--Widgets Section-->
	<div class="widgets-section">
		<div class="container">
			<div class="row">
				<!--Big Column-->
				<div class="big-column col-md-6 col-sm-12 col-xs-12">
					<div class="row clearfix">

						<!--Footer Column-->
						<div class="col-md-6 col-sm-6 col-xs-12">

							<div class="footer-widget about-column">
								<figure class="footer-logo"><a href="index.html"><img
												src="<?php echo base_url() ?>assets/images/logo/NatbioLogo.jpeg"
												alt=""></a></figure>
								<ul class="contact-info">
									<li><span class="icon-signs"></span>Calle México, Nro. 1485, Piso 1. Esq. Nicolas
										Acosta
										<br>La Paz - Bolivia
									</li>

									<li><span class="icon-phone-call"></span> Telefono: (+591) 22597952</li>
									<li><span class="icon-note"></span>natbio-srl@natbiosrl.com</li>
								</ul>
							</div>
						</div>
						<!--Footer Column-->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget link-column">
								<div class="section-title">
									<h4 style="color: #033E8A">Accesos Rápidos</h4>
								</div>
								<div class="widget-content">
									<ul class="list">
										<li><a href="<?php echo base_url() ?>index.php/welcome/aboutus">¿Quienes
												Somos?</a></li>
										<li><a href="<?php echo base_url() ?>index.php/welcome/gallery">Galeria</a>
										<li><a href="<?php echo base_url() ?>index.php/welcome/contacts">Contactos</a>
										</li>
									</ul>
								</div>
							</div>
							<p>Nuestras Redes Sociales</p>

							<ul class="social-icon">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-feed"></i></a></li>
								<li><a href="#"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!--Big Column-->
				<div class="big-column col-md-6 col-sm-12 col-xs-12">
					<div class="row clearfix">

						<!--Footer Column-->
						<div class="col-md-6 col-sm-6 col-xs-12">

						</div>

						<!--Footer Column-->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="footer-widget contact-column">
								<div class="section-title">
									<h4 style="color: #033E8A">Acceso Natbio-Srl</h4>
								</div>
								<h5><a href="https://www.natbiosrl.com:2096/webmaillogout.cgi" target="_blank">Revisar Webmail</a></h5>
								<p>Solo para personal de Natbio Srl</p>

							</div>
						</div>


					</div>
				</div>

			</div>
		</div>
	</div>


</footer>

<!--Footer Bottom-->
<section class="footer-bottom" style="background-color: #023E8A">
	<div class="container">
		<div class="pull-left copy-text">
			<p><a href="#" style="color: white">Copyrights © 2020</a> Todos los derechos reservados Natbio SRL.
				Desarrolloado por <a
						href="http://www.techsquad.xyz" target="_blank" style="color: white">TechSquad.</a></p>
		</div><!-- /.pull-right -->

	</div><!-- /.container -->
</section>

<!-- Scroll Top  -->
<button class="scroll-top tran3s " style="background-color: #EDD382; color: #023E8A"><span
			class="fa fa-angle-up"></span></button>
<!-- preloader  -->
<div class="preloader"></div>
<div id="donate-popup" class="donate-popup">
	<div class="close-donate theme-btn"><span class="fa fa-close"></span></div>
	<div class="popup-inner">


		<div class="container">
			<div class="donate-form-area">
				<div class="section-title center">
					<h2>Donation Information</h2>
				</div>

				<h4>How much would you like to donate:</h4>

				<form action="#" class="donate-form default-form">
					<ul class="chicklet-list clearfix">
						<li>
							<input type="radio" id="donate-amount-1" name="donate-amount"/>
							<label for="donate-amount-1" data-amount="1000">$1000</label>
						</li>
						<li>
							<input type="radio" id="donate-amount-2" name="donate-amount" checked="checked"/>
							<label for="donate-amount-2" data-amount="2000">$2000</label>
						</li>
						<li>
							<input type="radio" id="donate-amount-3" name="donate-amount"/>
							<label for="donate-amount-3" data-amount="3000">$3000</label>
						</li>
						<li>
							<input type="radio" id="donate-amount-4" name="donate-amount"/>
							<label for="donate-amount-4" data-amount="4000">$4000</label>
						</li>
						<li>
							<input type="radio" id="donate-amount-5" name="donate-amount"/>
							<label for="donate-amount-5" data-amount="5000">$5000</label>
						</li>
						<li class="other-amount">

							<div class="input-container" data-message="Every dollar you donate helps end hunger.">
								<span>Or</span><input type="text" id="other-amount" placeholder="Other Amount"/>
							</div>
						</li>
					</ul>

					<h3>Donor Information</h3>

					<div class="form-bg">
						<div class="row clearfix">
							<div class="col-md-6 col-sm-6 col-xs-12">

								<div class="form-group">
									<p>Your Name*</p>
									<input type="text" name="fname" placeholder="">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<p>Email*</p>
									<input type="text" name="fname" placeholder="">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<p>Address*</p>
									<input type="text" name="fname" placeholder="">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<p>Phn Num*</p>
									<input type="text" name="fname" placeholder="">
								</div>
							</div>

						</div>
					</div>

					<ul class="payment-option">
						<li>
							<h4>Choose your payment method:</h4>
						</li>
						<li>
							<div class="checkbox">
								<label>
									<input name="pay-us" type="checkbox">
									<span>Paypal</span>
								</label>
							</div>
						</li>
						<li>
							<div class="checkbox">
								<label>
									<input name="pay-us" type="checkbox">
									<span>Offline Donation</span>
								</label>
							</div>
						</li>
						<li>
							<div class="checkbox">
								<label>
									<input name="pay-us" type="checkbox">
									<span>Credit Card</span>
								</label>
							</div>
						</li>
						<li>
							<div class="checkbox">
								<label>
									<input name="pay-us" type="checkbox">
									<span>Debit Card</span>
								</label>
							</div>
						</li>
					</ul>

					<div class="center">
						<button class="thm-btn" type="submit">Donate Now</button>
					</div>


				</form>
			</div>
		</div>


	</div>
</div>


<!-- jQuery -->
<script src=" <?php echo base_url() ?>assets/js/jquery.js"></script>
<script src=" <?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src=" <?php echo base_url() ?>assets/js/menu.js"></script>
<script src=" <?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.mixitup.min.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.fancybox.pack.js"></script>
<script src=" <?php echo base_url() ?>assets/js/imagezoom.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.polyglot.language.switcher.js"></script>
<script src=" <?php echo base_url() ?>assets/js/SmoothScroll.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.appear.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.countTo.js"></script>
<script src=" <?php echo base_url() ?>assets/js/validation.js"></script>
<script src=" <?php echo base_url() ?>assets/js/wow.js"></script>
<script src=" <?php echo base_url() ?>assets/js/jquery.fitvids.js"></script>
<script src=" <?php echo base_url() ?>assets/js/nouislider.js"></script>
<script src=" <?php echo base_url() ?>assets/js/isotope.js"></script>
<script src=" <?php echo base_url() ?>assets/js/pie-chart.js"></script>


<!-- revolution slider js -->
<script src="<?php echo base_url() ?>assets/js/rev-slider/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/rev-slider/revolution.extension.video.min.js"></script>


<script src="<?php echo base_url() ?>assets/js/custom.js"></script>

</div>

</body>
</html>
