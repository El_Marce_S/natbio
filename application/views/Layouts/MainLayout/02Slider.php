<!--Start rev slider wrapper-->
<section class="rev_slider_wrapper">
	<div id="slider1" class="rev_slider" data-version="5.0">
		<ul>

			<li data-transition="fade">
				<img src="assets/images/slider/agua.jpg" alt="" width="1920" height="888" data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="15"
					 data-y="top" data-voffset="260"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">

						<h3>«Convertid un árbol en leña <br>y arderá para vosotros,<br> pero no producirá flores ni
							frutos
							para vuestros hijos»</h3>
						<span>
							Rabindranath Tagore
						</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="15"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">

				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="200"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/cieslo112.jpg" alt="" width="1920" height="580"
					 data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="center" data-hoffset=""
					 data-y="top" data-voffset="100"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box center">

						<p>
						<h3>«Produce una inmensa tristeza pensar <br>que la naturaleza habla mientras <br>el género
							humano
							no la escucha»</h3> </p>
						<span>Victor Hugo</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="center" data-hoffset="-90"
					 data-y="top" data-voffset="450"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="center" data-hoffset="100"
					 data-y="top" data-voffset="450"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/cieslo112fds.jpg" alt="" width="1920" height="888"
					 data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="100"
					 data-y="top" data-voffset="260"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">

						<h3>La tierra proporciona lo suficiente <br>para satisfacer las necesidades de cada hombre, <br>pero
							no la codicia de cada hombre</h3>
						<span>
							Mahatma Gandhi
						</span>

					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="500"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="700"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/climate-2584730_960_720.jpg" alt="" width="1920" height="888"
					 data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="500"
					 data-y="top" data-voffset="260"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">

						<h3>«Dos cosas me llaman la atención: <br> la inteligencia de las bestias <br>y la bestialidad
							de
							los hombres»</h3>
						<span>Flora Tristán</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="500"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="700"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/cobweb-921039_960_720.jpg" alt="" width="1920" height="888"
					 data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="200"
					 data-y="top" data-voffset="100"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">
						<h3>«El mundo es un lugar peligroso, <br> no a causa de los que hacen el mal <br> sino por
							aquellos
							<br>
							que no hacen nada para evitarlo» </h3>
						<span>Albert Einstein</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="200"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="700"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/LAGO.jpg" alt="" width="1920" height="888" data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="200"
					 data-y="top" data-voffset="260"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">
						<h3>La tierra no es una herencia de nuestros padres, <br>sino un préstamo de nuestros hijos
						</h3>
						<span>Proverbio indio</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="500"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="700"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>
			<li data-transition="fade">
				<img src="assets/images/slider/rough-horn-2146181_960_720.jpg" alt="" width="1920" height="888"
					 data-bgposition="top center"
					 data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">

				<div class="tp-caption  tp-resizeme"
					 data-x="left" data-hoffset="200"
					 data-y="top" data-voffset="100"
					 data-transform_idle="o:1;"
					 data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="700">
					<div class="slide-content-box">
						<h3>Cuando el ultimo árbol sea cortado, <br>el último rio envenenado, <br>el ultimo pez pescado,
							<br> solo entonces el hombre descubrirá <br> que el dinero no se come </h3>
						<span>Proverbio Cree</span>
					</div>
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="500"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2300">
				</div>
				<div class="tp-caption tp-resizeme"
					 data-x="left" data-hoffset="700"
					 data-y="top" data-voffset="480"
					 data-transform_idle="o:1;"
					 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
					 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
					 data-splitin="none"
					 data-splitout="none"
					 data-responsive_offset="on"
					 data-start="2600">

				</div>
			</li>


		</ul>
	</div>
</section>
<!--End rev slider wrapper-->


<section class="urgent-cause2 sec-padd " style="background-color: #90E0EF" ;>
	<div class="container">
		<div class="section-title">
			<h2>Galeria</h2>
			<p style="color: #0a0a0a">Algunas fotos de nuestro trabajos...</p>
		</div>
		<div class="cause-carousel">
			<?php
			$files = scandir('assets/images/GALERIA/');
			foreach ($files as $file) {
				if ($file == '..' || $file == '...' || $file == '.') {
					null;
				} else {
					echo('
					<div class="item clearfix">
						<div style="color: white">
							<figure class="img-box">
								<img src="assets/images/GALERIA/' . $file . '" alt="" style="height: 240px; width: 370px">
								<form action="' . base_url() . 'index.php/welcome/singlePicture/' . $file . '">
								<div class="overlay" style="">
										<div class="inner-box">
											<div class="content-box">
												<button class="thm-btn style-2 ">Ver mas..</button>
											</div>
										</div>
									</div>
								</form>
							</figure>
						</div>
					</div>'
					);
				}
			}
			?>
		</div>
	</div>
</section>
