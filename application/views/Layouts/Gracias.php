<html lang="en-US" class="chrome chrome79" style="overflow-x: hidden;">
<head>
	<style class="darkreader darkreader--fallback" media="screen"></style>
	<style class="darkreader darkreader--text" media="screen"></style>
	<style class="darkreader darkreader--invert" media="screen"></style>
	<style class="darkreader darkreader--inline" media="screen">[data-darkreader-inline-bgcolor] {
			background-color: var(--darkreader-inline-bgcolor) !important;
		}

		[data-darkreader-inline-bgimage] {
			background-image: var(--darkreader-inline-bgimage) !important;
		}

		[data-darkreader-inline-border] {
			border-color: var(--darkreader-inline-border) !important;
		}

		[data-darkreader-inline-border-bottom] {
			border-bottom-color: var(--darkreader-inline-border-bottom) !important;
		}

		[data-darkreader-inline-border-left] {
			border-left-color: var(--darkreader-inline-border-left) !important;
		}

		[data-darkreader-inline-border-right] {
			border-right-color: var(--darkreader-inline-border-right) !important;
		}

		[data-darkreader-inline-border-top] {
			border-top-color: var(--darkreader-inline-border-top) !important;
		}

		[data-darkreader-inline-boxshadow] {
			box-shadow: var(--darkreader-inline-boxshadow) !important;
		}

		[data-darkreader-inline-color] {
			color: var(--darkreader-inline-color) !important;
		}

		[data-darkreader-inline-fill] {
			fill: var(--darkreader-inline-fill) !important;
		}

		[data-darkreader-inline-stroke] {
			stroke: var(--darkreader-inline-stroke) !important;
		}

		[data-darkreader-inline-outline] {
			outline-color: var(--darkreader-inline-outline) !important;
		}</style>
	<style class="darkreader darkreader--user-agent" media="screen">html {
			background-color: #181a1b !important;
		}

		html, body, input, textarea, select, button {
			background-color: #181a1b;
		}

		html, body, input, textarea, select, button {
			border-color: #575757;
			color: #e8e6e3;
		}

		a {
			color: #3391ff;
		}

		table {
			border-color: #4c4c4c;
		}

		::placeholder {
			color: #bab5ab;
		}

		::selection {
			background-color: #005ccc;
			color: #ffffff;
		}

		::-moz-selection {
			background-color: #005ccc;
			color: #ffffff;
		}

		input:-webkit-autofill,
		textarea:-webkit-autofill,
		select:-webkit-autofill {
			background-color: #545b00 !important;
			color: #e8e6e3 !important;
		}</style>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<title>Gracias</title>
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//s.w.org">
	<link href="https://fonts.gstatic.com" crossorigin="" rel="preconnect">
	<link rel="alternate" type="application/rss+xml" title="Ultimate Addons Template » Feed"
		  href="https://templates.ultimatebeaver.com/feed/">
	<link rel="alternate" type="application/rss+xml" title="Ultimate Addons Template » Comments Feed"
		  href="https://templates.ultimatebeaver.com/comments/feed/">
	<script type="text/javascript">
		window._wpemojiSettings = {
			"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
			"ext": ".png",
			"svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
			"svgExt": ".svg",
			"source": {"concatemoji": "https:\/\/templates.ultimatebeaver.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.2"}
		};
		!function (e, a, t) {
			var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d");

			function c(e, t) {
				var a = String.fromCharCode;
				s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
				var r = p.toDataURL();
				return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
			}

			function l(e) {
				if (!s || !s.fillText) return !1;
				switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
					case"flag":
						return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]));
					case"emoji":
						return !c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])
				}
				return !1
			}

			function d(e) {
				var t = a.createElement("script");
				t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
			}

			for (i = Array("flag", "emoji"), t.supports = {
				everything: !0,
				everythingExceptFlag: !0
			}, o = 0; o < i.length; o++) t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]);
			t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () {
				t.DOMReady = !0
			}, t.supports.everything || (n = function () {
				t.readyCallback()
			}, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () {
				"complete" === a.readyState && t.readyCallback()
			})), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji)))
		}(window, document, window._wpemojiSettings);
	</script>
	<script src="https://templates.ultimatebeaver.com/wp-includes/js/wp-emoji-release.min.js?ver=5.3.2"
			type="text/javascript" defer=""></script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="astra-theme-css-css"
		  href="https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/css/minified/style.min.css?ver=1.6.4"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<style id="astra-theme-css-inline-css" type="text/css">
		html {
			font-size: 87.5%;
		}

		a, .page-title {
			color: #808285;
		}

		a:hover, a:focus {
			color: #3a3a3a;
		}

		body, button, input, select, textarea {
			font-family: 'Raleway', sans-serif;
			font-weight: 500;
			font-size: 14px;
			font-size: 1rem;
			line-height: 1.8;
		}

		blockquote {
			border-color: rgba(128, 130, 133, 0.05);
		}

		h1, .entry-content h1, .entry-content h1 a, h2, .entry-content h2, .entry-content h2 a, h3, .entry-content h3, .entry-content h3 a, h4, .entry-content h4, .entry-content h4 a, h5, .entry-content h5, .entry-content h5 a, h6, .entry-content h6, .entry-content h6 a, .site-title, .site-title a {
			font-family: 'Raleway', sans-serif;
			font-weight: normal;
		}

		.site-title {
			font-size: 35px;
			font-size: 2.5rem;
		}

		.ast-archive-description .ast-archive-title {
			font-size: 40px;
			font-size: 2.8571428571429rem;
		}

		.site-header .site-description {
			font-size: 15px;
			font-size: 1.0714285714286rem;
		}

		.entry-title {
			font-size: 30px;
			font-size: 2.1428571428571rem;
		}

		.comment-reply-title {
			font-size: 23px;
			font-size: 1.6428571428571rem;
		}

		.ast-comment-list #cancel-comment-reply-link {
			font-size: 14px;
			font-size: 1rem;
		}

		h1, .entry-content h1, .entry-content h1 a {
			font-size: 52px;
			font-size: 3.7142857142857rem;
		}

		h2, .entry-content h2, .entry-content h2 a {
			font-size: 50px;
			font-size: 3.5714285714286rem;
		}

		h3, .entry-content h3, .entry-content h3 a {
			font-size: 30px;
			font-size: 2.1428571428571rem;
		}

		h4, .entry-content h4, .entry-content h4 a {
			font-size: 20px;
			font-size: 1.4285714285714rem;
		}

		h5, .entry-content h5, .entry-content h5 a {
			font-size: 14px;
			font-size: 1rem;
		}

		h6, .entry-content h6, .entry-content h6 a {
			font-size: 14px;
			font-size: 1rem;
		}

		.ast-single-post .entry-title, .page-title {
			font-size: 30px;
			font-size: 2.1428571428571rem;
		}

		#secondary, #secondary button, #secondary input, #secondary select, #secondary textarea {
			font-size: 14px;
			font-size: 1rem;
		}

		::selection {
			background-color: #808285;
			color: #000000;
		}

		body, h1, .entry-title a, .entry-content h1, .entry-content h1 a, h2, .entry-content h2, .entry-content h2 a, h3, .entry-content h3, .entry-content h3 a, h4, .entry-content h4, .entry-content h4 a, h5, .entry-content h5, .entry-content h5 a, h6, .entry-content h6, .entry-content h6 a {
			color: #808285;
		}

		.tagcloud a:hover, .tagcloud a:focus, .tagcloud a.current-item {
			color: #000000;
			border-color: #808285;
			background-color: #808285;
		}

		.main-header-menu a, .ast-header-custom-item a {
			color: #808285;
		}

		.main-header-menu li:hover > a, .main-header-menu li:hover > .ast-menu-toggle, .main-header-menu .ast-masthead-custom-menu-items a:hover, .main-header-menu li.focus > a, .main-header-menu li.focus > .ast-menu-toggle, .main-header-menu .current-menu-item > a, .main-header-menu .current-menu-ancestor > a, .main-header-menu .current_page_item > a, .main-header-menu .current-menu-item > .ast-menu-toggle, .main-header-menu .current-menu-ancestor > .ast-menu-toggle, .main-header-menu .current_page_item > .ast-menu-toggle {
			color: #808285;
		}

		input:focus, input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="reset"]:focus, input[type="search"]:focus, textarea:focus {
			border-color: #808285;
		}

		input[type="radio"]:checked, input[type=reset], input[type="checkbox"]:checked, input[type="checkbox"]:hover:checked, input[type="checkbox"]:focus:checked, input[type=range]::-webkit-slider-thumb {
			border-color: #808285;
			background-color: #808285;
			box-shadow: none;
		}

		.site-footer a:hover + .post-count, .site-footer a:focus + .post-count {
			background: #808285;
			border-color: #808285;
		}

		.footer-adv .footer-adv-overlay {
			border-top-style: solid;
			border-top-color: #7a7a7a;
		}

		.ast-comment-meta {
			line-height: 1.666666667;
			font-size: 11px;
			font-size: 0.78571428571429rem;
		}

		.single .nav-links .nav-previous, .single .nav-links .nav-next, .single .ast-author-details .author-title, .ast-comment-meta {
			color: #808285;
		}

		.menu-toggle, button, .ast-button, .button, input#submit, input[type="button"], input[type="submit"], input[type="reset"] {
			border-radius: 2px;
			padding: 10px 40px;
			color: #ffffff;
			border-color: #f7b91a;
			background-color: #f7b91a;
		}

		button:focus, .menu-toggle:hover, button:hover, .ast-button:hover, .button:hover, input[type=reset]:hover, input[type=reset]:focus, input#submit:hover, input#submit:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="submit"]:hover, input[type="submit"]:focus {
			color: #ffffff;
			border-color: #333333;
			background-color: #333333;
		}

		.entry-meta, .entry-meta * {
			line-height: 1.45;
			color: #808285;
		}

		.entry-meta a:hover, .entry-meta a:hover *, .entry-meta a:focus, .entry-meta a:focus * {
			color: #3a3a3a;
		}

		blockquote, blockquote a {
			color: #35373a;
		}

		.ast-404-layout-1 .ast-404-text {
			font-size: 200px;
			font-size: 14.285714285714rem;
		}

		.widget-title {
			font-size: 20px;
			font-size: 1.4285714285714rem;
			color: #808285;
		}

		#cat option, .secondary .calendar_wrap thead a, .secondary .calendar_wrap thead a:visited {
			color: #808285;
		}

		.secondary .calendar_wrap #today, .ast-progress-val span {
			background: #808285;
		}

		.secondary a:hover + .post-count, .secondary a:focus + .post-count {
			background: #808285;
			border-color: #808285;
		}

		.calendar_wrap #today > a {
			color: #000000;
		}

		.ast-pagination a, .page-links .page-link, .single .post-navigation a {
			color: #808285;
		}

		.ast-pagination a:hover, .ast-pagination a:focus, .ast-pagination > span:hover:not(.dots), .ast-pagination > span.current, .page-links > .page-link, .page-links .page-link:hover, .post-navigation a:hover {
			color: #3a3a3a;
		}

		.ast-header-break-point .ast-mobile-menu-buttons-minimal.menu-toggle {
			background: transparent;
			color: #f7b91a;
		}

		.ast-header-break-point .ast-mobile-menu-buttons-outline.menu-toggle {
			background: transparent;
			border: 1px solid #f7b91a;
			color: #f7b91a;
		}

		.ast-header-break-point .ast-mobile-menu-buttons-fill.menu-toggle {
			background: #f7b91a;
			color: #ffffff;
		}

		@media (min-width: 545px) {
			.ast-page-builder-template .comments-area, .single.ast-page-builder-template .entry-header, .single.ast-page-builder-template .post-navigation {
				max-width: 1260px;
				margin-left: auto;
				margin-right: auto;
			}
		}

		@media (max-width: 768px) {
			.ast-archive-description .ast-archive-title {
				font-size: 40px;
			}

			.entry-title {
				font-size: 30px;
			}

			h1, .entry-content h1, .entry-content h1 a {
				font-size: 30px;
			}

			h2, .entry-content h2, .entry-content h2 a {
				font-size: 25px;
			}

			h3, .entry-content h3, .entry-content h3 a {
				font-size: 20px;
			}

			.ast-single-post .entry-title, .page-title {
				font-size: 30px;
			}
		}

		@media (max-width: 544px) {
			.ast-archive-description .ast-archive-title {
				font-size: 40px;
			}

			.entry-title {
				font-size: 30px;
			}

			h1, .entry-content h1, .entry-content h1 a {
				font-size: 30px;
			}

			h2, .entry-content h2, .entry-content h2 a {
				font-size: 25px;
			}

			h3, .entry-content h3, .entry-content h3 a {
				font-size: 20px;
			}

			.ast-single-post .entry-title, .page-title {
				font-size: 30px;
			}
		}

		@media (max-width: 768px) {
			html {
				font-size: 79.8%;
			}
		}

		@media (max-width: 544px) {
			html {
				font-size: 79.8%;
			}
		}

		@media (min-width: 769px) {
			.ast-container {
				max-width: 1260px;
			}
		}

		@font-face {
			font-family: "Astra";
			src: url(https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/fonts/astra.woff) format("woff"), url(https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/fonts/astra.ttf) format("truetype"), url(https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/fonts/astra.svg#astra) format("svg");
			font-weight: normal;
			font-style: normal;
		}

		@media (max-width: 921px) {
			.main-header-bar .main-header-bar-navigation {
				display: none;
			}
		}

		.ast-desktop .main-header-menu.submenu-with-border .sub-menu, .ast-desktop .main-header-menu.submenu-with-border .children, .ast-desktop .main-header-menu.submenu-with-border .astra-full-megamenu-wrapper {
			border-color: #eaeaea;
		}

		.ast-desktop .main-header-menu.submenu-with-border .sub-menu, .ast-desktop .main-header-menu.submenu-with-border .children {
			border-top-width: 1px;
			border-right-width: 1px;
			border-left-width: 1px;
			border-bottom-width: 1px;
			border-style: solid;
		}

		.ast-desktop .main-header-menu.submenu-with-border .sub-menu .sub-menu, .ast-desktop .main-header-menu.submenu-with-border .children .children {
			top: -1px;
		}

		.ast-desktop .main-header-menu.submenu-with-border .sub-menu a, .ast-desktop .main-header-menu.submenu-with-border .children a {
			border-bottom-width: 1px;
			border-style: solid;
			border-color: #eaeaea;
		}

		@media (min-width: 769px) {
			.main-header-menu .sub-menu li.ast-left-align-sub-menu:hover > ul, .main-header-menu .sub-menu li.ast-left-align-sub-menu.focus > ul {
				margin-left: -2px;
			}
		}

		@media (max-width: 920px) {
			.ast-404-layout-1 .ast-404-text {
				font-size: 100px;
				font-size: 7.1428571428571rem;
			}
		}

		.ast-header-break-point .site-header {
			border-bottom-width: 1px;
		}

		@media (min-width: 769px) {
			.main-header-bar {
				border-bottom-width: 1px;
			}
		}

		.ast-flex {
			-webkit-align-content: center;
			-ms-flex-line-pack: center;
			align-content: center;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-moz-box-align: center;
			-ms-flex-align: center;
			align-items: center;
		}

		.main-header-bar {
			padding: 1em 0;
		}

		.ast-site-identity {
			padding: 0;
		}

		@media (min-width: 769px) {
			.ast-theme-transparent-header #masthead {
				position: absolute;
				left: 0;
				right: 0;
			}

			.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header.ast-header-break-point .main-header-bar {
				background: none;
			}

			body.elementor-editor-active.ast-theme-transparent-header #masthead, .fl-builder-edit .ast-theme-transparent-header #masthead, body.vc_editor.ast-theme-transparent-header #masthead {
				z-index: 0;
			}

			.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .ast-mobile-header-logo {
				display: none;
			}

			.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .transparent-custom-logo .custom-logo {
				display: inline-block;
			}

			.ast-theme-transparent-header .ast-above-header {
				background-image: none;
				background-color: transparent;
			}

			.ast-theme-transparent-header .ast-below-header {
				background-image: none;
				background-color: transparent;
			}
		}

		@media (max-width: 768px) {
			.ast-theme-transparent-header #masthead {
				position: absolute;
				left: 0;
				right: 0;
			}

			.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header.ast-header-break-point .main-header-bar {
				background: none;
			}

			body.elementor-editor-active.ast-theme-transparent-header #masthead, .fl-builder-edit .ast-theme-transparent-header #masthead, body.vc_editor.ast-theme-transparent-header #masthead {
				z-index: 0;
			}

			.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .ast-mobile-header-logo {
				display: none;
			}

			.ast-header-break-point.ast-replace-site-logo-transparent.ast-theme-transparent-header .transparent-custom-logo .custom-logo {
				display: inline-block;
			}

			.ast-theme-transparent-header .ast-above-header {
				background-image: none;
				background-color: transparent;
			}

			.ast-theme-transparent-header .ast-below-header {
				background-image: none;
				background-color: transparent;
			}
		}

		.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header .site-header {
			border-bottom-width: 0;
		}
	</style>
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="astra-google-fonts-css"
		  href="//fonts.googleapis.com/css?family=Raleway%3A500%2Cnormal%2C%2C900%2C400%7COpen+Sans+Condensed%3A700&amp;ver=1.6.4"
		  type="text/css" media="all">
	<link rel="stylesheet" id="wp-block-library-css"
		  href="https://templates.ultimatebeaver.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.2"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="font-awesome-5-css"
		  href="https://templates.ultimatebeaver.com/wp-content/plugins/bb-plugin/fonts/fontawesome/css/all.min.css?ver=2.2.6.2"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="font-awesome-css"
		  href="https://templates.ultimatebeaver.com/wp-content/plugins/bb-plugin/fonts/fontawesome/css/v4-shims.min.css?ver=2.2.6.2"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="fl-builder-layout-13625-css"
		  href="https://templates.ultimatebeaver.com/wp-content/uploads/bb-plugin/cache/13625-layout.css?ver=2e2ff68edd66953d3604e14ed33a4c6d"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="astra-addon-css-css"
		  href="https://templates.ultimatebeaver.com/wp-content/uploads/astra-addon/astra-addon-5dcbff6f970949-24624745.css?ver=2.1.4"
		  type="text/css" media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<style id="astra-addon-css-inline-css" type="text/css">
		h1, .entry-content h1 {
			color: #000000;
		}

		h2, .entry-content h2 {
			color: #000000;
		}

		h3, .entry-content h3 {
			color: #000000;
		}

		h4, .entry-content h4 {
			color: #000000;
		}

		h5, .entry-content h5 {
			color: #808285;
		}

		h6, .entry-content h6 {
			color: #0a0a0a;
		}

		.ast-separate-container .blog-layout-1, .ast-separate-container .blog-layout-2, .ast-separate-container .blog-layout-3 {
			background-color: transparent;
			background-image: none;
		}

		.ast-separate-container .ast-article-post {
			background-color: #ffffff;
		}

		.ast-separate-container .ast-article-single, .ast-separate-container .comment-respond, .ast-separate-container .ast-comment-list li, .ast-separate-container .ast-woocommerce-container, .ast-separate-container .error-404, .ast-separate-container .no-results, .single.ast-separate-container .ast-author-meta, .ast-separate-container .related-posts-title-wrapper, .ast-separate-container.ast-two-container #secondary .widget, .ast-separate-container .comments-count-wrapper, .ast-box-layout.ast-plain-container .site-content, .ast-padded-layout.ast-plain-container .site-content {
			background-color: #ffffff;
		}

		.site-title, .site-title a {
			font-family: 'Raleway', sans-serif;
			text-transform: inherit;
		}

		.site-header .site-description {
			text-transform: inherit;
		}

		.secondary .widget-title {
			font-family: 'Raleway', sans-serif;
			text-transform: inherit;
		}

		.secondary .widget > *:not(.widget-title) {
			font-family: 'Raleway', sans-serif;
		}

		.ast-single-post .entry-title, .page-title {
			font-family: 'Raleway', sans-serif;
			text-transform: inherit;
		}

		.ast-archive-description .ast-archive-title {
			font-family: 'Raleway', sans-serif;
			text-transform: inherit;
		}

		.blog .entry-title, .blog .entry-title a, .archive .entry-title, .archive .entry-title a, .search .entry-title, .search .entry-title a {
			font-family: 'Raleway', sans-serif;
			text-transform: inherit;
		}

		h1, .entry-content h1, .entry-content h1 a {
			font-weight: 900;
			font-family: 'Raleway', sans-serif;
			line-height: 1.6;
			text-transform: inherit;
		}

		h2, .entry-content h2, .entry-content h2 a {
			font-weight: 900;
			font-family: 'Raleway', sans-serif;
			line-height: 1;
			text-transform: inherit;
		}

		h3, .entry-content h3, .entry-content h3 a {
			font-weight: 700;
			font-family: 'Open Sans Condensed', sans-serif;
			line-height: 1;
			text-transform: uppercase;
		}

		h4, .entry-content h4, .entry-content h4 a {
			font-weight: 700;
			font-family: 'Open Sans Condensed', sans-serif;
			line-height: 1.3;
			text-transform: uppercase;
		}

		h5, .entry-content h5, .entry-content h5 a {
			font-weight: 400;
			font-family: 'Raleway', sans-serif;
			line-height: 1.6;
			text-transform: inherit;
		}

		h6, .entry-content h6, .entry-content h6 a {
			font-weight: 700;
			font-family: 'Open Sans Condensed', sans-serif;
			line-height: 1.3;
			text-transform: inherit;
		}
	</style>
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="astra-child-theme-css-css"
		  href="https://templates.ultimatebeaver.com/wp-content/themes/astra-child/style.css?ver=1.0.0" type="text/css"
		  media="all">
	<style class="darkreader darkreader--sync" media="screen"></style>
	<link rel="stylesheet" id="fl-builder-google-fonts-8d2d266dfe5b5d0a6a7c426ce4d834d6-css"
		  href="//fonts.googleapis.com/css?family=Playfair+Display%3Anormal%2C700%7CMontserrat%3Anormal&amp;ver=5.3.2"
		  type="text/css" media="all">
	<!--[if IE]>
	<script type='text/javascript'
			src='https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/js/minified/flexibility.min.js?ver=1.6.4'></script>
	<script type='text/javascript'>
		flexibility(document.documentElement);
	</script>
	<![endif]-->
	<script type="text/javascript">
		/* <![CDATA[ */
		var uabb = {"ajax_url": "https:\/\/templates.ultimatebeaver.com\/wp-admin\/admin-ajax.php"};
		/* ]]> */
	</script>
	<style class="darkreader darkreader--override" media="screen">.jfk-bubble {
			background-color: #000000 !important;
		}

		.vimvixen-hint {
			background-color: #7b5300 !important;
			border-color: #d8b013 !important;
			color: #f3e8c8 !important;
		}

		::placeholder {
			opacity: 0.5 !important;
		}</style>
	<script type="text/javascript"
			src="https://templates.ultimatebeaver.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
	<script type="text/javascript"
			src="https://templates.ultimatebeaver.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
	<link rel="https://api.w.org/" href="https://templates.ultimatebeaver.com/wp-json/">
	<link rel="EditURI" type="application/rsd+xml" title="RSD"
		  href="https://templates.ultimatebeaver.com/xmlrpc.php?rsd">
	<link rel="wlwmanifest" type="application/wlwmanifest+xml"
		  href="https://templates.ultimatebeaver.com/wp-includes/wlwmanifest.xml">
	<meta name="generator" content="WordPress 5.3.2">
	<link rel="canonical" href="https://templates.ultimatebeaver.com/landing/thank-you/">
	<link rel="shortlink" href="https://templates.ultimatebeaver.com/?p=13625">
	<link rel="alternate" type="application/json+oembed"
		  href="https://templates.ultimatebeaver.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftemplates.ultimatebeaver.com%2Flanding%2Fthank-you%2F">
	<link rel="alternate" type="text/xml+oembed"
		  href="https://templates.ultimatebeaver.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftemplates.ultimatebeaver.com%2Flanding%2Fthank-you%2F&amp;format=xml">
	<style type="text/css">

		.fl-page-nav-right.fl-page-header-fixed {
			/* top: 32px; */
			display: none !important;
		}

		.site-header {
			z-index: 10;
			position: relative;
			display: none;
		}

		#primary, #secondary {
			margin: 0 0 0;
		}

		.next-scroll-top {
			display: none !important;
		}

		.main-header-bar-wrap {
			display: none;
		}

		.site-footer {
			display: none;
		}

		.page-header-image-single.grid-container.grid-parent {
			display: none;
		}

		div#right-sidebar {
			display: none;
		}

		div#primary {
			width: 100%;
		}

		body .grid-container {
			max-width: none;
		}

		.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content {
			padding: 0px !important;
		}
	</style>
	<style class="darkreader darkreader--sync" media="screen"></style>
	<!-- Stream WordPress user activity plugin v3.4.2 -->
</head>

<body itemtype="https://schema.org/WebPage" itemscope="itemscope"
	  class="page-template-default page page-id-13625 page-child parent-pageid-13524 fl-builder ast-desktop ast-page-builder-template ast-no-sidebar astra-1.6.4 ast-header-custom-item-inside group-blog ast-single-post ast-inherit-site-logo-transparent ast-normal-title-enabled astra-addon-2.1.4"
	  style="">

<div id="page" class="hfeed site">


	<header itemtype="https://schema.org/WPHeader" itemscope="itemscope" id="masthead"
			class="site-header header-main-layout-1 ast-primary-menu-disabled ast-no-menu-items ast-menu-toggle-icon ast-mobile-header-inline"
			role="banner">


	</header><!-- #masthead -->


	<div id="content" class="site-content">

		<div class="ast-container">


			<div id="primary" class="content-area primary">


				<main id="main" class="site-main" role="main">


					<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-13625"
							 class="post-13625 page type-page status-publish has-post-thumbnail ast-article-single">


						<header class="entry-header ast-no-meta">


							<div class="entry-content clear" itemprop="text">


								<div
									class="fl-builder-content fl-builder-content-13625 fl-builder-content-primary fl-builder-global-templates-locked"
									data-post-id="13625">
									<div
										class="fl-row fl-row-full-width fl-row-bg-photo fl-node-5853c8e60c984 fl-row-bg-overlay"
										data-node="5853c8e60c984">
										<div class="fl-row-content-wrap">
											<div class="uabb-row-separator uabb-top-row-separator">
											</div>
											<div class="fl-row-content fl-row-fixed-width fl-node-content">

												<div class="fl-col-group fl-node-5853c8e60c791"
													 data-node="5853c8e60c791">
													<div class="fl-col fl-node-5853c8e60c8a6" data-node="5853c8e60c8a6">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-spacer-gap fl-node-5853c8e60c8de"
																data-node="5853c8e60c8de">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-spacer-gap-preview uabb-spacer-gap">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="fl-col-group fl-node-5853c8e60c757"
													 data-node="5853c8e60c757">
													<div class="fl-col fl-node-5853c8e60c800" data-node="5853c8e60c800">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-info-box fl-node-5853c8e60c86f"
																data-node="5853c8e60c86f">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-infobox infobox-center ">
																		<div class="uabb-infobox-left-right-wrap">
																			<div class="uabb-infobox-content">
																				<div class="uabb-infobox-title-wrap"><h1
																						class="uabb-infobox-title">
																						Gracias.</h1></div>
																				<div class="uabb-infobox-text-wrap">
																					<div
																						class="uabb-infobox-text uabb-text-editor">
																						<p>Nos pondremos pronto en
																							contacto contigo. Por favor,
																							verifica tu bandeja de
																							entrada y la bandeja de
																							spam o correo no deseado</p>
																					</div>
																					<div class="uabb-infobox-button">
																						<div
																							class="uabb-module-content uabb-button-wrap uabb-creative-button-wrap uabb-button-width-custom uabb-creative-button-width-custom">

																						</div>


																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="fl-col-group fl-node-5853c8e60c7c8"
													 data-node="5853c8e60c7c8">
													<div class="fl-col fl-node-5853c8e60c915" data-node="5853c8e60c915">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-spacer-gap fl-node-5853c8e60cb07"
																data-node="5853c8e60cb07">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-spacer-gap-preview uabb-spacer-gap">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="fl-row fl-row-full-width fl-row-bg-color fl-node-5853c8e60c9bb"
										 data-node="5853c8e60c9bb">
										<div class="fl-row-content-wrap">
											<div class="fl-row-content fl-row-fixed-width fl-node-content">

												<div class="fl-col-group fl-node-5853c8e60cb3f"
													 data-node="5853c8e60cb3f">
													<div class="fl-col fl-node-5853c8e60cb76" data-node="5853c8e60cb76">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-spacer-gap fl-node-5853c8e60cbae"
																data-node="5853c8e60cbae">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-spacer-gap-preview uabb-spacer-gap">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="fl-col-group fl-node-5853c8e60c9f3"
													 data-node="5853c8e60c9f3">
													<div class="fl-col fl-node-5853c8e60ca99 fl-col-small"
														 data-node="5853c8e60ca99">
														<div class="fl-col-content fl-node-content">
														</div>
													</div>
													<div class="fl-col fl-node-5853c8e60ca2a" data-node="5853c8e60ca2a">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-info-box fl-node-5853c8e60ca61"
																data-node="5853c8e60ca61">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-infobox infobox-center ">
																		<div class="uabb-infobox-left-right-wrap">
																			<div class="uabb-infobox-content">

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="fl-col fl-node-5853c8e60cad0 fl-col-small"
														 data-node="5853c8e60cad0">
														<div class="fl-col-content fl-node-content">
														</div>
													</div>
												</div>


												<div class="fl-col-group fl-node-5853c8e60cbe5"
													 data-node="5853c8e60cbe5">
													<div class="fl-col fl-node-5853c8e60cc1c" data-node="5853c8e60cc1c">
														<div class="fl-col-content fl-node-content">
															<div
																class="fl-module fl-module-spacer-gap fl-node-5853c8e60c94c fl-visible-medium"
																data-node="5853c8e60c94c">
																<div class="fl-module-content fl-node-content">
																	<div
																		class="uabb-module-content uabb-spacer-gap-preview uabb-spacer-gap">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uabb-js-breakpoint" style="display: none;"></div>


							</div><!-- .entry-content .clear -->


					</article><!-- #post-## -->


				</main><!-- #main -->


			</div><!-- #primary -->


		</div> <!-- ast-container -->

	</div><!-- #content -->


	<footer itemtype="https://schema.org/WPFooter" itemscope="itemscope" id="colophon" class="site-footer"
			role="contentinfo">


	</footer><!-- #colophon -->


</div><!-- #page -->


<script type="text/javascript"></script>
<script type="text/javascript">
	/* <![CDATA[ */
	var astra = {"break_point": "921", "isRtl": ""};
	/* ]]> */
</script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/themes/astra/assets/js/minified/style.min.js?ver=1.6.4"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/plugins/bb-plugin/js/jquery.infinitescroll.min.js?ver=2.2.6.2"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/plugins/bb-plugin/js/jquery.mosaicflow.js?ver=2.2.6.2"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/plugins/bb-ultimate-addon/assets/js/global-scripts/jquery-masonary.js?ver=5.3.2"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/plugins/bb-ultimate-addon/assets/js/global-scripts/jquery-carousel.js?ver=5.3.2"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/uploads/bb-plugin/cache/13625-layout.js?ver=2e2ff68edd66953d3604e14ed33a4c6d"></script>
<script type="text/javascript">
	/* <![CDATA[ */
	var astraAddon = {"sticky_active": ""};
	/* ]]> */
</script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-content/uploads/astra-addon/astra-addon-5dcbff6f9aa4d8-49013939.js?ver=2.1.4"></script>
<script type="text/javascript"
		src="https://templates.ultimatebeaver.com/wp-includes/js/wp-embed.min.js?ver=5.3.2"></script>


</body>
</html>
