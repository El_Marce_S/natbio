<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Welcome extends CI_Controller


{


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()


	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed

		$this->load->helper('url');
		$this->load->helper('directory');

		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/02Slider');
		$this->load->view('Layouts/MainLayout/33Footer');
	}


	public function aboutus()
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/aboutus');
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function gallery()
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/gallery');
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function singlePicture($picname)

	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$data['picname'] = $picname;
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/singlePic', $data);
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function FileBrowse()

	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed

		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/FileBrowse');
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function SingleFolder($MainFolderName)
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$data['foldername'] = $MainFolderName;
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/MainFolderExplorer', $data);
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function SubFolder001($ParentFolder, $SubFolderName001)
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$data['foldername'] = $SubFolderName001;
		$data['parentfolder'] = $ParentFolder;
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/SubFolder001', $data);
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function SubFolder002($GandParentFolder, $ParentFolder, $SubFolderName002)
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed

		$data['foldername'] = $SubFolderName002;
		$data['parentfolder'] = $ParentFolder;
		$data['GrandParentFolder'] = $GandParentFolder;
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/SubFolder002', $data);
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function contacts()
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed

		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/contact');
		$this->load->view('Layouts/MainLayout/33Footer');
	}

	public function servicesIndustrial()
	{
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$this->load->view('Layouts/MainLayout/0Header');
		$this->load->view('Layouts/MainLayout/01NavBar');
		$this->load->view('Layouts/MainLayout/servicesIndustrial');
		$this->load->view('Layouts/MainLayout/33Footer');
	}


	public function nuevomsg()
	{
		$this->load->librHeader('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowedary('email');
		date_default_timezone_set('America/La_Paz');
		$now = date('Y-m-d');
		$time = date(' H:i:s');
		//configuracion para mail
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.natbiosrl.com',
			'smtp_user' => 'info@@natbiosrl.com', //Su Correo de Gmail Aqui
			'smtp_pass' => 'natbio2021*', // Su Password de Gmail aqui
			'smtp_port' => '465',
			'smtp_crypto' => 'tls',
			'mailtype' => 'html',
			'wordwrap' => TRUE,
			'charset' => 'utf-8'
		);
		Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
		Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
		Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
		$this->email->from('info@natbiosrl.com', 'Correo de Informacion');
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->subject($_POST['subject']);
		$this->email->message($_POST['message'] . ' ' . '<<<<<<<<<<<<<<<<<<<<<<<<<-------------------------->>>>>>>>>>>>>>>>>>>>>>>>> El mensaje anterior fue enviado desde la direccion ' . '  ' . $_POST['email'] . '  Perteneciente a ' . $_POST['name']);
		$this->email->to('natbio-srl@natbiosrl.com');
		$this->email->cc('fsuarez@natbiosrl.com');
		$this->email->cc('info@natbiosrl.com');
		$this->email->cc('mpenaranda@natbiosrl.com');
		$this->email->send();
		$this->email->set_newline("\r\n");
		$this->email->from('info@natbiosrl.com');
		$this->email->subject('Gracias por su mensaje');
		$this->email->message('Muchas gracias por ponerte en contacto con nosotros. Pronto, alguien de nuestro equipo se pondra en contacto contigo');
		$this->email->to($_POST['email']);
		$this->email->send();
		$this->load->view('Gracias');
		redirect('/welcome/contacts');
	}
}
